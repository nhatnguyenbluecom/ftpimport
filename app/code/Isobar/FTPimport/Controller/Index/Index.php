<?php

namespace Isobar\FTPimport\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $cron;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Isobar\FTPimport\Model\Cron $cron
    ) {
        $this->cron = $cron;
        parent::__construct($context);
    }

    /**
     * Example index action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->cron->execute();
        die('Finish test');
    }
}