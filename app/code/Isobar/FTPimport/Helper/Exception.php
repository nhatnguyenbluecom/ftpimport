<?php
namespace Isobar\FTPimport\Helper;
class Exception extends \Magento\Framework\App\Helper\AbstractHelper
{
    const LOG_FILE_PATH = '/var/log/Isobar_FTPimport.log';

    public function log($data, $logFilePath = null)
    {
        $filePath = $logFilePath ? $logFilePath : self::LOG_FILE_PATH;
        $writer = new \Zend\Log\Writer\Stream(BP . $filePath);
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($data, true));
    }
}
