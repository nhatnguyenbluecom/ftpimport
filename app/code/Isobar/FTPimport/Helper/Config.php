<?php
namespace Isobar\FTPimport\Helper;
use Magento\Framework\App\Config\ScopeConfigInterface;
class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    CONST HOST = 'host';
    CONST USER = 'user'; //for ftp
    CONST USERNAME = 'username'; // for sftp
    CONST PASSWORD = 'password';
    CONST FILE_PATH = 'file_path';
    CONST FILE_NAME = 'file_name';
    CONST SFTP = 'sftp';

    protected $scopeConfig;
    protected $storeManager;
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;

    }

    public function getInfoStoreConfig($key, $encrypt = false)
    {
        $value = $this->scopeConfig->getValue('isobar_ftpimport_config/general/' . $key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($encrypt) {
            return $this->_encryptor->decrypt($value);
        }
        return $value;
    }

    public function getFtpConfigData()
    {
        return [
            self::HOST => $this->getInfoStoreConfig(self::HOST),
            self::USER => $this->getInfoStoreConfig(self::USER),
            self::PASSWORD => $this->getInfoStoreConfig(self::PASSWORD),
            //'ssl' => false,
            'passive' => true,
            'sftp' => true
        ];
    }

    public function getSftpConfigData()
    {
        return [
            self::HOST => $this->getInfoStoreConfig(self::HOST),
            self::USERNAME => $this->getInfoStoreConfig(self::USER),
            self::PASSWORD => $this->getInfoStoreConfig(self::PASSWORD),
        ];
    }

    public function getFileName()
    {
        return $this->getInfoStoreConfig(self::FILE_NAME);
    }

    public function getFilePath()
    {
        return $this->getInfoStoreConfig(self::FILE_PATH);
    }

    public function getProtocalType()
    {
        if ($this->getInfoStoreConfig(self::SFTP)) {
            return 'sftp';
        }
        return 'ftp';
    }
}
