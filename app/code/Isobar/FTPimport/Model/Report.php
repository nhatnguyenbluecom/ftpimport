<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 12/06/2017
 * Time: 15:04
 */

namespace Isobar\FTPimport\Model;


class Report extends \Magento\Framework\Model\AbstractModel implements \Isobar\FTPimport\Api\Data\ReportInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Isobar\FTPimport\Model\ResourceModel\Report');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->_getData('id');
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        return $this->setData('id', $id);
    }

    /**
     * {@inheritdoc}
     */
    public function getFileName()
    {
        return $this->_getData(self::FILE_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setFileName($fileName)
    {
        return $this->setData(self::FILE_NAME, $fileName);
    }

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        return $this->_getData(self::CONTENT);
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->_getData(self::STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        return $this->setData('status', $status);
    }

    /**
     * {@inheritdoc}
     */
    public function getDate() {
        return $this->_getData(self::DATE);
    }

    /**
     * {@inheritdoc}
     */
    public function setDate($date) {
        return $this->setData(self::DATE, $date);
    }
}
