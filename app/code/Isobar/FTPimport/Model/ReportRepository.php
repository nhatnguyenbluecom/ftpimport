<?php

namespace Isobar\FTPimport\Model;

class ReportRepository implements \Isobar\FTPimport\Api\ReportRepositoryInterface {
    /**
     * @var \Isobar\FTPimport\Model\ReportFactory
     */
    protected $modelFactory;

    /**
     * @var \Isobar\FTPimport\Model\ResourceModel\Report
     */
    protected $resourceModel;

    /**
     * @var \Isobar\FTPimport\Model\ResourceModel\Report\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Isobar\FTPimport\Api\Data\ReportSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Isobar\FTPimport\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * ReportRepository constructor.
     * @param ReportFactory $modelFactory
     * @param ResourceModel\Report $resourceModel
     * @param ResourceModel\Report\CollectionFactory $collectionFactory
     * @param \Isobar\FTPimport\Api\Data\ReportSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Isobar\FTPimport\Model\ReportFactory $modelFactory,
        \Isobar\FTPimport\Model\ResourceModel\Report $resourceModel,
        \Isobar\FTPimport\Model\ResourceModel\Report\CollectionFactory $collectionFactory,
        \Isobar\FTPimport\Api\Data\ReportSearchResultsInterfaceFactory $searchResultsFactory,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->modelFactory = $modelFactory;
        $this->resourceModel = $resourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Isobar\FTPimport\Api\Data\ReportInterface $model) {
        try {
            $this->resourceModel->save($model);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__('Unable to save ftp report'));
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function get($modelId) {
        $model = $this->modelFactory->create();
        $this->resourceModel->load($model, $modelId);
        if(!$model->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested report doesn\'t exist'));
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($id)
    {
        $report = $this->modelFactory->create();
        $this->resourceModel->load($report, $id);
        if (!$report->getId()) {
            throw new NoSuchEntityException(__('Report with id "%1" does not exist.', $id));
        }
        return $report;
    }
    /**
     * {@inheritdoc}
     */
    public function delete(\Isobar\FTPimport\Api\Data\ReportInterface $model) {
        $id = $model->getId();
        try {
            $this->resourceModel->delete($model);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(__('Unable to remove report %1', $id));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($modelId) {
        $model = $this->get($modelId);
        return $this->delete($model);
    }

    /**
     * {@inheritdoc}
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria) {
        /** @var \Isobar\FTPimport\Model\ResourceModel\Report\Collection $collection */
        $collection = $this->collectionFactory->create();

        //Add filters from root filter group to the collection
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }

        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $field = $sortOrder->getField();
            $collection->addOrder(
                $field,
                ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
            );
        }

        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->load();

        /** @var \Isobar\FTPimport\Api\Data\ReportSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Isobar\FTPimport\Model\ResourceModel\Report\Collection $collection
     * @return void
     */
    private function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Isobar\FTPimport\Model\ResourceModel\Report\Collection $collection)
    {
        $fields = [];
        $conditions = [];

        foreach($filterGroup->getFilters() as $filter){
            $field = $filter->getField();
            $condition = $filter->getConditionType() ?: 'eq';
            $value = $filter->getValue();

            $fields[] = $field;
            $conditions[] = [ $condition => $value ];
        }

        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }
}