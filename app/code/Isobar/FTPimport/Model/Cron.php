<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 12/06/2017
 * Time: 15:04
 */

namespace Isobar\FTPimport\Model;
class Cron
{
    CONST DIRECTORY_TYPE = 'var';
    CONST IMPORT_FOLDER = 'ftp_import/';

    /**
     * @var FTPServer\Connection
     */
    protected $ftpConnection;

    /**
     * @var mixed
     */
    protected $fileName;

    /**
     * @var mixed
     */
    protected $filePath;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    /**
     * @var Import\Product
     */
    protected $productImport;

    /**
     * @var \Isobar\FTPimport\Helper\Exception
     */
    protected $ftpException;

    /**
     * @var \Isobar\FTPimport\Api\Data\ReportInterfaceFactory
     */
    protected $reportFactory;

    /**
     * @var ReportRepository
     */
    protected $reportRepository;

    /**
     * Cron constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param FTPServer\Connection $ftpConnection
     * @param \Isobar\FTPimport\Helper\Config $ftpConfig
     * @param Import\Product $productImport
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Isobar\FTPimport\Helper\Exception $ftpException
     * @param \Isobar\FTPimport\Api\Data\ReportInterfaceFactory $reportFactory
     * @param ReportRepository $reportRepository
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Isobar\FTPimport\Model\FTPServer\Connection $ftpConnection,
        \Isobar\FTPimport\Helper\Config $ftpConfig,
        \Isobar\FTPimport\Model\Import\Product $productImport,
        \Magento\Framework\Filesystem $filesystem,
        \Isobar\FTPimport\Helper\Exception $ftpException,
        \Isobar\FTPimport\Api\Data\ReportInterfaceFactory $reportFactory,
        \Isobar\FTPimport\Model\ReportRepository $reportRepository
    ) {
        $this->ftpException = $ftpException;
        $this->fileSystem = $filesystem;
        $this->ftpConnection = $ftpConnection;
        $this->fileName = $ftpConfig->getFileName();
        $this->filePath = $ftpConfig->getFilePath();
        $this->productImport = $productImport;
        $this->reportFactory = $reportFactory;
        $this->reportRepository = $reportRepository;
    }

    /**
     * @throws \Exception
     */
    public function execute() {
        $reportData = ['status' => 0, 'file_name' => $this->fileName, 'content' => 'Can not access to ftp servier'];
        $this->ftpException->log('Start cron ...!');
        $connection = $this->ftpConnection->getConnection();
        if ($connection['status']) {
            $ftp = $connection['connection'];
            if ($this->filePath) {
                $ftp->cd($this->filePath);
            }
            try {
                $csvContent = $ftp->read($this->fileName);
                $this->_writeToFile($csvContent);
                $reportContent = $this->productImport->install(self::IMPORT_FOLDER . $this->fileName);
                $reportData['status'] = 1;
                $reportData['content'] = $reportContent;
                $this->ftpException->log($reportContent);
                $ftp->close();
            } catch (\Exception $e) {
                $this->ftpException->log($e->getMessage());
                $reportData['content'] = $e->getMessage();
            }
        } else {
            $this->ftpException->log('Can not access to ftp servier');
            $reportData['content'] = $connection['error'];
        }
        $this->_report($reportData);
    }

    /**
     * @param $reportData
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    protected function _report($reportData)
    {
        $report = $this->reportFactory->create();
        $reportData['date'] = (new \DateTime())->getTimestamp();
        $report->setData($reportData);
        $this->reportRepository->save($report);
    }

    /**
     * @param $csvContent
     */
    protected function _writeToFile($csvContent)
    {
        $writer = $this->fileSystem->getDirectoryWrite(self::DIRECTORY_TYPE );
        $writer->writeFile(self::IMPORT_FOLDER . $this->fileName , $csvContent );
    }
}