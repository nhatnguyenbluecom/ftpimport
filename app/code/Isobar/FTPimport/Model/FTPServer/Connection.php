<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 12/06/2017
 * Time: 15:04
 */

namespace Isobar\FTPimport\Model\FTPServer;
class Connection
{
    protected $ftpConfig;

    protected $ftp;

    protected $sftp;

    protected $ftpException;

    public function __construct(
        \Isobar\FTPimport\Helper\Config $ftpConfig,
        \Isobar\FTPimport\Helper\Exception $ftpException,
        \Magento\Framework\Filesystem\Io\Ftp $ftp,
        \Magento\Framework\Filesystem\Io\Sftp $sftp
    ) {
        $this->ftpConfig = $ftpConfig;
        $this->ftp = $ftp;
        $this->sftp = $sftp;
        $this->ftpException = $ftpException;
    }

    public function getConnection()
    {

        $result = ['status' => 0, 'error' => 'Can not access to ftp server'];
        $protocalType = $this->ftpConfig->getProtocalType();
        try {
            if ('sftp' === $protocalType) {
                $this->sftp->open($this->ftpConfig->getSftpConfigData());
                $result['connection'] = $this->sftp;
            } else {
                $this->ftp->open($this->ftpConfig->getFtpConfigData());
                $result['connection'] = $this->ftp;
            }
            $result['status'] = 1;
        } catch (\Exception $e) {
            $this->ftpException->log($e->getMessage());
            $result['error'] = $e->getMessage();
            //throw new \Exception($e->getMessage());
        }
        return $result;
    }
}