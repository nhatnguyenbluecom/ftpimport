<?php

namespace Isobar\FTPimport\Model\ResourceModel\Report;


class Collection extends \Isobar\FTPimport\Model\ResourceModel\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * _contruct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Isobar\FTPimport\Model\Report', 'Isobar\FTPimport\Model\ResourceModel\Report');
    }
}