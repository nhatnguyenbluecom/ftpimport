<?php

namespace Isobar\FTPimport\Model\ResourceModel;


class Report extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('isobar_ftp_report', 'id');
    }
}