<?php

namespace Isobar\FTPimport\Api;


interface ReportRepositoryInterface
{
    /**
     * @param \Isobar\FTPimport\Api\Data\ReportInterface $report
     * @return int
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Isobar\FTPimport\Api\Data\ReportInterface $report);

    /**
     * Get info about report by id
     *
     * @param int $modelId
     * @return \Isobar\FTPimport\Api\Data\ReportInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($modelId);

    /**
     * Retrieve report.
     *
     * @param int $id
     * @return \Isobar\FTPimport\Api\Data\ReportInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Delete report
     *
     * @param \Isobar\FTPimport\Api\Data\ReportInterface $report
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Isobar\FTPimport\Api\Data\ReportInterface $report);

    /**
     * Delete report by id
     *
     * @param int $id
     * @return bool will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($id);

    /**
     * Get report list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Isobar\FTPimport\Api\Data\ReportSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}