<?php

namespace Isobar\FTPimport\Api\Data;

/**
 * @api
 */
interface ReportSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Isobar\FTPimport\Api\Data\ReportInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Isobar\FTPimport\Api\Data\ReportInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
