<?php

namespace Isobar\FTPimport\Api\Data;


interface ReportInterface
{
    /**
     * Constants defined for keys of data array
     */
    const FILE_NAME         = 'file_name';
    const CONTENT           = 'content';
    const DATE              = 'date';
    const STATUS            = 'status';

    /**
     * Get megamenu id
     * @return int|null
     */
    public function getId();

    /**
     * Set megamenu id
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get report file name
     * @return string|null
     */
    public function getFileName();

    /**
     * Set report file name
     * @param string $fileName
     * @return $this
     */
    public function setFileName($fileName);

    /**
     * Get report content
     * @return string|null
     */
    public function getContent();

    /**
     * Set report content
     * @param string $content
     * @return $this
     */
    public function setContent($content);

    /**
     * Set report status
     * @param int $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get report status
     * @return int|null
     */
    public function getStatus();

    /**
     * Get report date
     *
     * @return string|null
     */
    public function getDate();

    /**
     * Set report date
     *
     * @param string $date
     * @return $this
     */
    public function setDate($date);
}